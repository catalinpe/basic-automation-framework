package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationPage {
    private final WebDriver driver;
    WebDriverWait wait;

    //fields
    @FindBy(id = "inputFirstName")
    WebElement firstNameInput;
    @FindBy(id = "inputLastName")
    WebElement lastNameInput;
    @FindBy(id = "inputEmail")
    WebElement emailAddressInput;
    @FindBy(id = "inputUsername")
    WebElement userNameCodeInput;
    @FindBy(id = "inputPassword")
    WebElement passwordCompleteInput;
    @FindBy(id = "inputPassword2")
    WebElement passwordConfirmInput;
    @FindBy(id = "register-submit")
    WebElement submitRegistrationButton;

    //erors
    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[3]/div/div[2]")
    WebElement errFirstName;

    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[4]/div/div[2]")
    WebElement errLastName;

    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[5]/div[1]/div[2]")
    WebElement errEmailAddress;

    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[6]/div/div[2]")
    WebElement errUserNameCode;

    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[7]/div/div[2]")
    WebElement errPasswordConfirm;

    @FindBy (how = How.XPATH, using = "//*[@id=\"registration_form\"]/div[8]/div/div[2]")
    WebElement errPasswordComplete;

    @FindBy(id="register-tab")
    WebElement openRegisterButton;

    public RegistrationPage (WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(this.driver, this);
    }

    public void registration (String firstname,String lastname, String email, String username, String passwordcomplete, String passwordconfirm) {
        firstNameInput.clear();
        firstNameInput.sendKeys(firstname);

        lastNameInput.clear();
        lastNameInput.sendKeys(lastname);

        emailAddressInput.clear();
        emailAddressInput.sendKeys(email);

        userNameCodeInput.clear();
        userNameCodeInput.sendKeys(username);

        passwordCompleteInput.clear();
        passwordCompleteInput.sendKeys(passwordcomplete);

        passwordConfirmInput.clear();
        passwordConfirmInput.sendKeys(passwordconfirm);
    }

    public void waitForRegistrationPage() {
        wait.until(ExpectedConditions.elementToBeClickable(submitRegistrationButton));
    }

    public boolean checkErr(String error, String type) {
        if (type.equalsIgnoreCase("firstNameErr"))
            return error.equals(errFirstName.getText());
        else if (type.equalsIgnoreCase("lastNameErr"))
            return error.equalsIgnoreCase(errLastName.getText());
        else if (type.equalsIgnoreCase("emailAddressErr"))
            return error.equalsIgnoreCase(errEmailAddress.getText());
        else if (type.equalsIgnoreCase("userNameCodeErr"))
            return error.equalsIgnoreCase(errUserNameCode.getText());
        else if (type.equalsIgnoreCase("passwordCompleteErr"))
            return error.equals(errPasswordComplete.getText());
        else if (type.equalsIgnoreCase("passwordConfirmErr"))
            return error.equals(errPasswordConfirm.getText());
        return false;
    }

    public void openRegistrationPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "/stubs/auth.html#registration_panel");
        driver.get(hostname + "/stubs/auth.html#registration_panel");
//        click on register button from Login page
        wait.until(ExpectedConditions.elementToBeClickable(openRegisterButton)).click();
    }
}
