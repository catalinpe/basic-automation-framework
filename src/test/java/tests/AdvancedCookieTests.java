package tests;

import Utils.OtherUtils;
import Utils.SeleniumUtils;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.sql.Driver;
import java.sql.SQLOutput;
import java.util.Set;

public class AdvancedCookieTests extends BaseTest {
    @Test
    public void cookieHomeworkTest () {
        driver.get(hostname + "/stubs/cookie.html");

        //Afiseaza toate cookies din pagina
        OtherUtils.printCookies (driver);

        try {
            OtherUtils.printCookie (driver.manage().getCookieNamed("myHomeworkCookie"));
        }
        catch (Exception e) {
            System.out.println("Are not cookies on the page "+hostname+"stubs/cookie.html");
        }

        //1. Create a cookie (from code – not using the browser)
        //Instantiez cookie
        Cookie cookie1 = new Cookie ("myHomeworkCookie", "my123cookie456", "Sat Feb 19 2022 20:57:04 GMT+0200 (Eastern European Standard Time)");

        //Adaug cookie
        driver.manage().addCookie(cookie1);

        //Afisez cookie adaugat
        OtherUtils.printCookie(cookie1);

        //Afisez toate cookies
        OtherUtils.printCookies(driver);

        //2. Open the page and ensure the previously created cookie is correctly read by the application
        //hostname http://86.121.249.149:4999
        driver.get(hostname + "/stubs/cookie.html");
        //Afiseaza cookie adaugat
        OtherUtils.printCookie(cookie1);
        //Afiseaza toate cookies
        OtherUtils.printCookies (driver);

        try {
            OtherUtils.printCookie (driver.manage().getCookieNamed("myHomeworkCookie"));
        }
        catch (Exception e) {
            System.out.println("Are not cookies on the page "+hostname+"stubs/cookie.html");
        }

    }

    @Test
    public void cookieHomeworkCheckTest () {
        driver.get(hostname + "/stubs/cookie.html");
        //Verific cookie din pagina, prin afisare
        System.out.println("Prima verificare (inainte de stergere): se afiseaza nr de cookie (ar trebui sa fie 1, cel adaugat):");
        OtherUtils.printCookies(driver); //cookies found 1

        //3. Click the “delete” cookie button
        //Actionez butonul "Remove the cookie"
        WebElement deleteCookie = driver.findElement(By.id("delete-cookie"));
        deleteCookie.click();

        //Sterg cookie adaugat
        driver.manage().deleteCookieNamed("myHomeworkCookie");

        //4. Check that the cookie was deleted (from code)
        //Verific ca s-a sters cookie
        System.out.println("A doua verificare (dupa stergere): se afiseaza nr de cookie (ar trebui sa fie 0):");
        OtherUtils.printCookies(driver); //cookies found 0

        //5. Check that the displayed value is correct (no value is displayed)
        //Verific daca mai sunt cookie
        System.out.println("Cookie in pagina mai sunt:");
        OtherUtils.printCookies(driver); //cookies found 0
    }
}
