package tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageObjects.RegistrationPage;
import pageObjects.ProfilePage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class RegistrationTests extends BaseTest {

    @DataProvider(name = "registerPositiveDp")
    public Iterator<Object[]> registerPositiveDp() {
        Collection<Object[]> dp = new ArrayList<Object[]>();
//       adauga aici ce elemente folosesti ptr regsiter
        // username, password
        dp.add(new String[]{"ab", "cd", "ab.cd@email.com", "abcd", "abcd1234", "abcd1234"});
        dp.add(new String[]{"ef", "gh", "ef.gh@email.com", "efgh", "efgh5678", "efgh5678"});
        dp.add(new String[]{"ij", "kl", "ij.kl@email.com", "ijkl", "ijkl1369", "ijkl1369"});
        dp.add(new String[]{"mn", "op", "mn.op@email.com", "mnop", "mnop2468", "mnop2468"});
        return dp.iterator();
    }

    @Test(dataProvider = "registerPositiveDp")
    public void registerPositiveTests(String firstname, String lastname, String email, String username, String passwordcomplete, String passwordconfirm) {
        RegistrationPage registrationPage = new RegistrationPage(driver);
//       aici nu este de ajuns doar sa deschizi pagina, trebuie sa faci si
//       click pe register ptr ca te duce in register page - vezi ca ti-am adaugat in openREgister page click-ul pe register button

        registrationPage.openRegistrationPage(hostname);

        registrationPage.registration(firstname, lastname, email, username, passwordcomplete, passwordconfirm);

        ProfilePage profilePage = new ProfilePage(driver);
        System.out.println("Logged in user :" + profilePage.getUser());
        Assert.assertEquals(username, profilePage.getUser());

        profilePage.logOut();
        //wait for registration page to be loaded
        registrationPage.waitForRegistrationPage();
    }
}